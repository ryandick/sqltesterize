module.exports = function(sequelize, DataTypes) {
  var Page = sequelize.define('Page', {
  
    pageID: {
      type: DataTypes.INTEGER,
      validate: {
        notNull: true,
        
        
        
      },
      
    },
  
    name: {
      type: DataTypes.STRING,
      validate: {
        notNull: true,
        
        
        
      },
      
    },
  
  })

  return Page
}
