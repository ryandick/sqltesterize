var express = require('express')

  , pages = require('./routes/pages')
  , http    = require('http')
  , path    = require('path')
  , db      = require('./models')

var app = express()

// all environments
app.set('port', process.env.PORT || 3000)
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(express.favicon())
app.use(express.logger('dev'))
app.use(express.json())
app.use(express.urlencoded())
app.use(express.methodOverride())
app.use(app.router)
app.use(express.static(path.join(__dirname, 'public')))

// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler())
}


app.get('/sqlTesterize/pages', pages.findAll)
app.get('/sqlTesterize/pages/:id', pages.find)
app.post('/sqlTesterize/pages', pages.create)
app.put('/sqlTesterize/pages/:id', pages.update)
app.delete('/sqlTesterize/pages/:id', pages.destroy)


db
  .sequelize
  .sync()
  .complete(function(err) {
    if (err) {
      throw err
    } else {
      http.createServer(app).listen(app.get('port'), function(){
        console.log('Express server listening on port ' + app.get('port'))
      })
    }
  })
