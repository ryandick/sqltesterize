'use strict';

angular.module('sqlTesterize')
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/pages', {
        templateUrl: 'views/page/pages.html',
        controller: 'PageController',
        resolve:{
          resolvedPage: ['Page', function (Page) {
            return Page.query();
          }]
        }
      })
    }]);
