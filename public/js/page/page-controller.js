'use strict';

angular.module('sqlTesterize')
  .controller('PageController', ['$scope', '$modal', 'resolvedPage', 'Page',
    function ($scope, $modal, resolvedPage, Page) {

      $scope.pages = resolvedPage;

      $scope.create = function () {
        $scope.clear();
        $scope.open();
      };

      $scope.update = function (id) {
        $scope.page = Page.get({id: id});
        $scope.open(id);
      };

      $scope.delete = function (id) {
        Page.delete({id: id},
          function () {
            $scope.pages = Page.query();
          });
      };

      $scope.save = function (id) {
        if (id) {
          Page.update({id: id}, $scope.page,
            function () {
              $scope.pages = Page.query();
              $scope.clear();
            });
        } else {
          Page.save($scope.page,
            function () {
              $scope.pages = Page.query();
              $scope.clear();
            });
        }
      };

      $scope.clear = function () {
        $scope.page = {
          
          "pageID": "",
          
          "name": "",
          
          "id": ""
        };
      };

      $scope.open = function (id) {
        var pageSave = $modal.open({
          templateUrl: 'page-save.html',
          controller: PageSaveController,
          resolve: {
            page: function () {
              return $scope.page;
            }
          }
        });

        pageSave.result.then(function (entity) {
          $scope.page = entity;
          $scope.save(id);
        });
      };
    }]);

var PageSaveController =
  function ($scope, $modalInstance, page) {
    $scope.page = page;

    

    $scope.ok = function () {
      $modalInstance.close($scope.page);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };
