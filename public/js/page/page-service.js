'use strict';

angular.module('sqlTesterize')
  .factory('Page', ['$resource', function ($resource) {
    return $resource('sqlTesterize/pages/:id', {}, {
      'query': { method: 'GET', isArray: true},
      'get': { method: 'GET'},
      'update': { method: 'PUT'}
    });
  }]);
